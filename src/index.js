import React from 'react';
import ReactDOM from 'react-dom';
import { Blogr } from './components/blogr/Blogr';
import './index.css';


// ALERT //
// THIS IMAGES SHOULD NOT GO HERE, CHECK
import iconClose from './assets/icon-close.svg';
import iconHamburger from './assets/icon-hamburger.svg';
// ALERT //


ReactDOM.render(
  <React.StrictMode>
    <Blogr />
  </React.StrictMode>,
  document.getElementById('root'),
);


// ALERT //
// THIS SHOULD NOT GO HERE, CHECK
let btn = document.getElementById("mobilemenu");
let mobilenav = document.getElementById("mobilenav");

mobilenav.style.left = "-100%";

btn.onclick = function () {
  if (mobilenav.style.left === "-100%") {
    mobilenav.style.left = "50%";
    btn.src = `${iconClose}`;
  } else {
    mobilenav.style.left = "-100%";
    btn.src = `${iconHamburger}`;
  }
}
// ALERT //







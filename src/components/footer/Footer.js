/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react'
import './styles.css';

// Images
import Logo from '../../assets/logo.svg';

export const Footer = () => {
    return (
        <>
            <footer>
                <div className="footer_box">
                    <a href="#"><img src={Logo} alt="Blogr logo" /></a>
                </div>
                <div className="footer_box">
                    <h6>Product</h6>
                    <ul>
                        <li><a href="#">Overview</a></li>
                        <li><a href="#">Pricing</a></li>
                        <li><a href="#">Marketplace</a></li>
                        <li><a href="#">Features</a></li>
                        <li><a href="#">Integrations</a></li>
                    </ul>
                </div>
                <div className="footer_box">
                    <h6>Company</h6>
                    <ul>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Team</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Careers</a></li>
                    </ul>
                </div>
                <div className="footer_box">
                    <h6>Connect</h6>
                    <ul>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Newsletter</a></li>
                        <li><a href="#">LinkedIn</a></li>
                    </ul>
                </div>

            </footer>
        </>
    )
}

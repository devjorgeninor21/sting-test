import React from 'react'
import { Description } from './description/Description';
import { Features } from './features/Features';
import { Services } from './services/Services';

export const Content = () => {

    return (
        <>
            <Description />
            <Services />
            <Features />
        </>
    )
}

import React, { useState, useEffect } from 'react'
import { ServicesDetail } from './services-detail/ServicesDetail';
import './styles.css';

// Images
import Phones from '../../../assets/illustration-phones.svg';
import Circles from '../../../assets/bg-pattern-circles.svg';

export const Services = () => {

    const [posts, setPosts] = useState([]);

    useEffect(() => {
        getData()
    })

    const getData = async () => {
        const url = 'https://posts-test-sting.vercel.app/api';
        // await fetch(url).then(resp => resp.json()).then(data => console.log(data));
        const resp = await fetch(url);
        const { data } = await resp.json();

        const posts = data.map(post => {
            return {
                id: post.id,
                title: post.title,
                body: post.body,
            }
        })

        setPosts(posts);
    }

    const getPost = (post) => {
        for (let i = 0; i <= post.length - 1; i++) {
            return <ServicesDetail key={post.id} {...post} />
        }
    }

    return (
        <>
            <section className="services">
                <div className="services_leftcolumn">
                    <img src={Phones} alt="phone illustration" className="phone" />
                    <img src={Circles} alt="circle pattern" className="circle_pattern" />
                </div>
                {
                    getPost(posts)
                }
            </section>
        </>
    )
}

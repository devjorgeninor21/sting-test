import React from 'react'
import './styles.css';

export const DescriptionDetail = (post) => {
    return (
        <div className="description_content_leftcolumn">
            {/* <h3>Introducing an extensible editor</h3>
            <p>Blogr features an exceedingly intuitive interface which lets you focus on one thing: creating content.
            The editor supports management of multiple blogs and allows easy manipulation of embeds such as images,
            videos, and Markdown. Extensibility with plugins and themes provide easy ways to add functionality or
            change the looks of a blog
                        </p>
            <h3>Robust content management</h3>
            <p>Flexible content management enables users to easily move through posts. Increase the usability of your blog
            by adding customized categories, sections, format, or flow. With this functionality, you’re in full control.
            </p> */}
            
            <h3>{post[0].title} </h3>
            <p>{post[0].body} </p>
        </div>
    )
}

import React from 'react'
import './styles.css';


export const FeatureDetail = (post) => {

    return (
        <div className="features_rightcolumn">
            {/* <h5>Free, open, simple</h5>
            <p>Blogr is a free and open source application backed by a large community of helpful developers. It supports
            features such as code syntax highlighting, RSS feeds, social media integration, third-party commenting tools,
            and works seamlessly with Google Analytics. The architecture is clean and is relatively easy to learn.
            </p>
            <h5>Powerful tooling</h5>
            <p>Batteries included. We built a simple and straightforward CLI tool that makes customization and deployment a breeze, but
            capable of producing even the most complicated sites.
            </p> */}

            <h5>{post[2].title} </h5>
            <p>{post[2].body} </p>
            <h5>{post[3].title} </h5>
            <p>{post[3].body} </p>
        </div>
    )
}

/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react'
import './styles.css';

// Images
import iconArrow from '../../../assets/icon-arrow-dark.svg';

export const HeaderMobile = () => {

    return (
        <>
            <section id="mobilenav" className="mobilenav">
                <ul>
                    <li className="connect">
                        <div className="connect_text">
                            <a href="#">Product</a>
                            <img src={iconArrow} alt="dropdown arrow" />
                        </div>
                        <div className="dropdown_menu_mobile">
                            <ul>
                                <li><a href="#">Overview</a></li>
                                <li><a href="#">Pricing</a></li>
                                <li><a href="#">Marketplace</a></li>
                                <li><a href="#">Features</a></li>
                                <li><a href="#">Integrations</a></li>
                            </ul>
                        </div>
                    </li>

                    <li className="connect">
                        <div className="connect_text">
                            <a href="#">Company</a>
                            <img src={iconArrow} alt="dropdown arrow" />
                        </div>
                        <div className="dropdown_menu_mobile">
                            <ul>
                                <li><a href="#">About</a></li>
                                <li><a href="#">Team</a></li>
                                <li><a href="#">Blog</a></li>
                                <li><a href="#">Careers</a></li>
                            </ul>
                        </div>
                    </li>

                    <li className="connect">
                        <div className="connect_text">
                            <a href="#">Connect</a>
                            <img src={iconArrow} alt="dropdown arrow" />
                        </div>
                        <div className="dropdown_menu_mobile">
                            <ul>
                                <li><a href="#">Contact</a></li>
                                <li><a href="#">Newsletter</a></li>
                                <li><a href="#">LinkedIn</a></li>
                            </ul>
                        </div>
                    </li>

                    <hr />
                    <li><a href="#">Login</a></li>
                    <li className="signup"><a href="#">Sign Up</a></li>
                </ul>
            </section>
        </>
    )
}
